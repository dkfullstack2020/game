import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.*;
import model.Goody;
import model.Health;

import java.awt.*;

public class Graphics {

    private GraphicsContext grafikH;
    private Model model;
    Image test = new Image("Images/animbg.gif", 0, 1200, true, true);
    Image player1 = new Image("Images/greenenemy.png", 100, 50, true, true);
    Image blue = new Image("Images/blue.png", 100, 50, true, true);
    Image hearth = new Image("Images/red-heart-png.png", 0, 1200, true, true);
    Image goody = new Image("Images/goody.gif", 0, 50, true, true);
    public Graphics(GraphicsContext gc, Model model) {
        this.grafikH = gc;
        this.model= model;
    }
    public void draw() {
        grafikH.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        grafikH.drawImage(test, 0, 000);
        Player s = model.getPlayer();
        grafikH.drawImage(player1, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getWidth(), model.getPlayer().getHeight());
        grafikH.setFill(Color.BEIGE);
        grafikH.setFont(Font.font("Arial", 26));
        grafikH.fillText("DK-Racer", 640, 50);
        grafikH.setFill(Color.GRAY);
        for (Enemy enemie : this.model.getEnemies()) {
            grafikH.drawImage(blue,
                    enemie.getX() - enemie.getW() / 2,
                    enemie.getY() - enemie.getH() / 2,
                    enemie.getW(),
                    enemie.getH()
            );
        }
        for (Goody goodie : this.model.getGoodies()) {
            grafikH.drawImage(goody,
                    goodie.getX() - goodie.getW() / 2,
                    goodie.getY() - goodie.getH() / 2,
                    goodie.getW(),
                    goodie.getH()
            );
        }

        grafikH.setFill(Color.RED);
        for (Health healthy : this.model.getHealth()) {
            grafikH.drawImage(hearth,
                    healthy.getX(),
                    healthy.getY(),
                    healthy.getWidth(),
                    healthy.getHeight()
            );
        }
        grafikH.setFont(new Font(25));
        grafikH.setFill(Color.BLACK);
        grafikH.fillText("Score" + model.getScore(), 1100, 600);
        grafikH.setFont(new Font(25));
        grafikH.setFill(Color.RED);
        grafikH.fillText("Lives" + model.getPlayer().getLives(), 1000, 20);
        if (model.isGameOver() == true) {
            grafikH.setFont(new Font(100));
            grafikH.setFill(Color.BLACK);
            grafikH.fillText("GAME OVER !!", 640, 360);
        }


        //grafikH.fillText("Time" + , 1000, 20);
       /* grafikH.setLineWidth(1.0);
        grafikH.setFill(Color.BLACK);
        grafikH.strokeText(model.getPlayer().getLives());*/


    }

}
