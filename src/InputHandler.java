import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {
    private Model model;

    public InputHandler (Model model){
        this.model = model;

    }

    public void onKeyPressed(KeyCode keycode){


        if(keycode== KeyCode.UP){
            model.getPlayer().move(0,-10);
        }

        if(keycode== KeyCode.DOWN){
            model.getPlayer().move(0,10);
        }



        if(keycode== KeyCode.LEFT){
            model.getPlayer().move(-6,0);
        }
        if(keycode== KeyCode.RIGHT){
            model.getPlayer().move(6,0);
        }

    }
}
