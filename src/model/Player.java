package model;

public class Player {
    private int x;
    private int y;
    private int height;
    private int width;
    private int lives = 5;

    public Player(int x, int y) {
        this.x = x;
        this.y= y;
        this.height= 100;
        this.width = 50;
        this.lives = 5;

    }

    //move methode
    public void move(int dx, int dy){
        this.x += dx;
        this.y += dy;
    }
    // kollison reset
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //player leben //TODO
    public int collisionbad() {
        this.lives -= 1;
        return lives;

    }

    //collect goodies
    public int collectgoodies() {
        this.lives += 1;
        return lives;
    }

    //getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getLives() {        return lives;
    }
}
