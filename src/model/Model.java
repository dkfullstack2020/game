package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    private Player player;
    private List<Enemy> enemies = new LinkedList<>();
    private List<model.Health> health = new ArrayList<>();
    private List<model.Goody> goodies = new LinkedList<>();
    private long attack = 1200;
    private int score = 0;
    private boolean gameOver = false;
    //enemy arraylist

    // private List<Goody> goodies = new LinkedList<>();

    public static final double WIDTH = 1280;
    public static final double HEIGHT = 720;

    //Konstruktor
    public Model() {
        // healt status ________________________________________________
        for (int i = 0; i < 5; i++) {
            this.health.add(
                    new model.Health(900 + (40 * i), 50));
        }


        //____________________________________________________________________________________________
        Random random = new Random();

        for (int i = 0; i < 2; i++) {
            this.enemies.add(
                    new Enemy(
                            random.nextInt(600),
                            random.nextInt(10),
                            0.08f + random.nextFloat() * 0.05f
                    )
            );
        }
//goodies
        for (int i = 0; i < 1; i++) {
            this.goodies.add(
                    new model.Goody(
                            random.nextInt(600),
                            random.nextInt(10),
                            0.08f + random.nextFloat() * 0.05f
                    )
            );
        }


        //player
        this.player = new Player(560, 600);

    }


    public Player getPlayer() {
        return player;
    }
    public List<Enemy> getEnemies() {
        return enemies;
    }

    public List<model.Goody> getGoodies() {
        return goodies;
    }

    public List<model.Health> getHealth() {
        return health;
    }







    // move methode


    public void update(long elapsedTime) {

        player.move(0, 0);

        //goodies
        for (model.Goody goody : goodies) {
            goody.update(elapsedTime);

            int dx = Math.abs(player.getX() - goody.getX());
            int dy = Math.abs(player.getY() - goody.getY());
            int w = player.getWidth() / 2 + goody.getW() / 2;
            int h = player.getHeight() / 2 + goody.getH() / 2;

            if (dx <= w && dy <= h) {
                // kollision!
                //player collisionbad
                //TODO wie ?   player.collisionbad();

                player.collectgoodies();

                //  health.add(new Health());


            }
            if (goody.isVisible() == false) {
                goodies.remove(goody);
                score -= 10;
            }
        }

        for (Enemy enemy : enemies) {
            enemy.update(elapsedTime);

            int dx = Math.abs(player.getX() - enemy.getX());
            int dy = Math.abs(player.getY() - enemy.getY());
            int w = player.getWidth() / 2 + enemy.getW() / 2;
            int h = player.getHeight() / 2 + enemy.getH() / 2;

            if (dx <= w && dy <= h) {
                // kollision!
                //player collisionbad
                //TODO wie ?   player.collisionbad();
                player.moveTo(player.getX() - 100, player.getY() + 10);
                player.collisionbad();

                health.remove(health.size() - 1);


            }
            if (enemy.isVisible() == false) {
                enemies.remove(enemy);
                score += 10;//
            }


            //to work on  important
            attack -= elapsedTime;
            if (attack <= 0) {
                Random random = new Random();
                //neue feinde
                for (int i = 0; i < 3; i++) {
                    this.enemies.add(
                            new Enemy(
                                    random.nextInt(400),
                                    random.nextInt(100),
                                    0.1f + random.nextFloat() * 0.05f
                            )
                    );
                }
                Enemy enemy1 = new Enemy(500, 0, 0.2f);
                Enemy enemy2 = new Enemy(random.nextInt(200), 0, 0.2f);
                Enemy enemy3 = new Enemy(random.nextInt(400), 0, 0.21f);
                enemies.add(enemy1);
                enemies.add(enemy2);
                enemies.add(enemy3);
                model.Goody goody11 = new model.Goody(500, 0, 0.2f);
                attack = 23000;
            }


        }
    }

    public int getScore() {
        return score;
    }

    public void gameOver() {
        if (player.getLives() < 0) {
            gameOver = true;
        }
    }

    public boolean isGameOver() {
        return gameOver;
    }
}
