import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Model;


import java.awt.*;

public class Main extends Application {
    private Model model;

    @Override
    public void start(Stage stage) throws Exception {
        Model model= new Model();

        Canvas canvas = new javafx.scene.canvas.Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group(); // sammelt mehr UI wir sollen aber nur im Canvas mit picel arbeiten

        group.getChildren().add(canvas);

        Scene scene = new Scene(group);
        stage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(gc, model);
        Timer timer = new Timer(graphics, model);
        timer.start();

        InputHandler inputHandler = new InputHandler((model));
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())); //event ist unsere selbstdefinierte variable
        stage.show();



    }
}
