package model;

public class Health {
    private int x;
    private int y;
    private int height;
    private int width;


    public Health(int x, int y) {
        this.x = x;
        this.y = y;
        this.height = 25;
        this.width = 25;


    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}

