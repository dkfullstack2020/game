package model;

import java.util.Random;

public class Goody {
    private int x;
    private int y;
    private float speedY;
    private boolean visible;
    private int score;


    public Goody(int x, int y, float speedY) {
        this.x = x;
        this.y = y;
        this.speedY = speedY;
        visible = true;
    }

    public void update(long elapsedTime) {
        Random random = new Random();
        this.y = Math.round(this.y + elapsedTime * this.speedY);
// auto umdrehen
        //  if(this.y > Model.HEIGHT && this.speedY > 0   ||  this.y < 0 && this.speedY < 0 ) {
        //       this.speedY = -1 * this.speedY;
        //   }
//auto verschwinden und score
        if (this.y > Model.HEIGHT) {
            visible = false;
            score -= 0;

        }

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 35;
    }

    public int getW() {
        return 35;
    }

    public float getSpeedY() {
        return speedY;
    }

    public boolean isVisible() {
        return visible;
    }

    public int getScore() {
        return score;
    }
}
