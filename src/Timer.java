import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {
    private long previousTime = -1;
    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model){
        this.graphics= graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        long nowMillis = nowNano / 1000000;

        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;
        model.update(elapsedTime);
        graphics.draw();
    }
}
//________________________________________________________________________________________________________________________


